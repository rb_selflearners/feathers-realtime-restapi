const feathers = require('@feathersjs/feathers');
const socketio = require('@feathersjs/socketio');

// Create a Feathers application
const app = feathers();

// Configure the Socket.io transport
app.configure(socketio());

// Start the server on port 3030
app.listen(3030);
